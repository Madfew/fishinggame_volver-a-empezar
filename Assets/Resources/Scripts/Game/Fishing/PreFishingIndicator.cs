﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PreFishingIndicator : MonoBehaviour
{
    public Image imgIndicator;
    public GameObject canvasIndicator;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowIndicator()
    {
        imgIndicator.DOFade(1f, 0.5f);
    }

    public void HideIndicator()
    {
        imgIndicator.DOFade(0f, 0.5f);
    }
}
