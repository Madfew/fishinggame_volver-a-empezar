﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreFishingController : MonoBehaviour
{
    public PreFishingIndicator indicator;
    public bool bActive;
    public bool bInZone;

    private Vector3 mousePosition;
    public Vector3 directionLaunch;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {
        if (!bActive)
        {
            indicator.HideIndicator();
            return;
        }
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit))
        {
            if (hit.transform.tag == "PreFishing")
            {
                bInZone = true;
                mousePosition = hit.point;
                directionLaunch = mousePosition;
                indicator.canvasIndicator.transform.position = mousePosition;
                indicator.ShowIndicator();
            }
            else
            {
                bInZone = false;
                indicator.HideIndicator();
            }
        }
    }
}
