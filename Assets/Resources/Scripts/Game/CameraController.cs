﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Camera cameraMain;
    public MenuController menuController;
    public float smoothSpeed;

    public float movePos;

    private bool bZoom;

    private void Start()
    {
        FindObjectOfType<AudioManager>().PlayMusic("Brisas Marinas");
        FindObjectOfType<AudioManager>().PlayMusic("Music");
    }
    void Update()
    {
        if (bZoom)
        {
            cameraMain.orthographicSize = Mathf.MoveTowards(cameraMain.orthographicSize, 8f, smoothSpeed * Time.deltaTime);
            cameraMain.transform.position = Vector3.MoveTowards(cameraMain.transform.position, new Vector3(cameraMain.transform.position.x, 10f, cameraMain.transform.position.z), smoothSpeed * Time.deltaTime);
            if (cameraMain.orthographicSize <= 8f)
            {
                PlayerController.Instance.bStart = true;
                PlayerController.Instance.preFishingController.bActive = true;
                bZoom = false;
            }
        }
    }

    public void MoveCameraToPosition(Vector3 nextPosition, float moveSpeed)
    {
        transform.position = Vector3.Lerp(transform.position, nextPosition, Time.deltaTime * moveSpeed);
    }

    public void MoveToGameplay()
    {
        bZoom = true;
    }
}
