﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;

    public PreFishingController preFishingController;
    //public RopeController ropeController;
    public BucketController bucketController;
    public CameraController cameraController;
    public MenuController menuController;

    public GameObject hookPrefab;
    public GameObject hookPrefabGame;
    public Transform spawnPoint;

    private HookController hookController;
    private float forceLaunch;
    private bool bSpawn;
    private float time;

    public Animator animPlayer;
    private string currentState;

    public Material matFace;
    public Texture[] faceSunflower;

    public bool bStart;

    private GameObject hookIni;

    enum Phase { Launch, Fishing};
    Phase myPhase;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        myPhase = Phase.Launch;
        matFace.mainTexture = faceSunflower[0];
    }

    void Update()
    {
        if (!bStart)
        {
            return;
        }

        Vector3 targetPosition = new Vector3(preFishingController.indicator.canvasIndicator.transform.position.x, transform.position.y, preFishingController.indicator.canvasIndicator.transform.position.z);
        transform.LookAt(targetPosition);

        if(myPhase == Phase.Launch)
        {
            if (!preFishingController.bInZone)
            {
                return;
            }
            if (Input.GetMouseButtonUp(0))
            {
                preFishingController.bActive = false;
                if (Vector3.Distance(preFishingController.directionLaunch, spawnPoint.transform.position) < 2f)
                {
                    time = 3f;
                    forceLaunch = 0.5f;
                }
                else if (Vector3.Distance(preFishingController.directionLaunch, spawnPoint.transform.position) < 2.5f)
                {
                    time = 3f;
                    forceLaunch = 1.2f;
                }
                else if (Vector3.Distance(preFishingController.directionLaunch, spawnPoint.transform.position) < 3f)
                {
                    time = 3f;
                    forceLaunch = 1.8f;
                }
                else if (Vector3.Distance(preFishingController.directionLaunch, spawnPoint.transform.position) < 3.5f)
                {
                    time = 3.5f;
                    forceLaunch = 2.7f;
                }
                else if (Vector3.Distance(preFishingController.directionLaunch, spawnPoint.transform.position) < 3.9f)
                {
                    time = 4f;
                    forceLaunch = 3.2f;
                }
                StartCoroutine(Launch());
            }
        }
        else if (myPhase == Phase.Fishing)
        {
            if (bSpawn)
            {
                StartCoroutine(SpawnHook());
                bSpawn = false;
            }
            if (CrossPlatformInputManager.GetButtonDown("Jump"))
            {
                matFace.mainTexture = faceSunflower[3];
                ChangeAnimationStage("Return");
                menuController.HideButtonChangeLaunch();
                StartCoroutine(ReturnSunflowerBase());
                Destroy(this.hookController.gameObject);
                Destroy(this.hookIni.gameObject);
            }
        }
    }

    IEnumerator Launch()
    {
        matFace.mainTexture = faceSunflower[1];
        ChangeAnimationStage("Launch");
        yield return new WaitForSeconds(0.8f);
        matFace.mainTexture = faceSunflower[2];
        menuController.ShowButtonChangeLaunch();
        GameObject hookInstance = Instantiate(hookPrefab, spawnPoint.position, Quaternion.identity);
        hookInstance.GetComponent<Rigidbody>().AddForce(transform.forward * forceLaunch, ForceMode.Impulse);
        hookIni = hookInstance;
        bSpawn = true;
        myPhase = Phase.Fishing;
    }

    IEnumerator SpawnHook()
    {
        yield return new WaitForSeconds(time);
        Vector3 posNew = new Vector3(hookIni.transform.position.x, hookIni.transform.position.y - 0.2f, hookIni.transform.position.z);
        GameObject hookInstance = Instantiate(hookPrefabGame, posNew, Quaternion.identity);
        hookController = hookInstance.GetComponent<HookController>();
        hookController.pos1 = hookIni.transform;
        hookController.line.enabled = false;
        yield return new WaitForSeconds(0.2f);
        hookController.line.enabled = true;
    }

    public void MakePoint()
    {
        matFace.mainTexture = faceSunflower[3];
        ChangeAnimationStage("Return");
        menuController.HideButtonChangeLaunch();
        StartCoroutine(GeneratePoint());
        StartCoroutine(ReturnSunflowerBase());
        Destroy(this.hookController.gameObject);
        Destroy(this.hookIni.gameObject);
    }

    IEnumerator GeneratePoint()
    {
        yield return new WaitForSeconds(1f);
        bucketController.SpawnPoint();
    }

    IEnumerator ReturnSunflowerBase()
    {
        yield return new WaitForSeconds(1f);
        matFace.mainTexture = faceSunflower[0];
        preFishingController.bActive = true;
        myPhase = Phase.Launch;
    }

    //-----------ANIMATOR CONTROLLER------------
    public void ChangeAnimationStage(string newState)
    {
        if (currentState == newState) return;
        animPlayer.Play(newState);
        currentState = newState;
    }
}
