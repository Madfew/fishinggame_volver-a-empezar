﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindManager : MonoBehaviour
{
    public GameObject[] spawnPointsList;
    private Transform transformSpawn;

    public GameObject[] windTrailList;
    private GameObject trailPrefab;

    float timerCooldown = 1f;
    float timer = 2.5f;

    private void Update()
    {
        timerCooldown -= Time.deltaTime;
        if(timerCooldown <= 0)
        {
            timerCooldown = timer;
            SpawnObject();
        }
    }

    public void SpawnObject()
    {
        transformSpawn = spawnPointsList[Random.Range(0, spawnPointsList.Length - 1)].transform;
        trailPrefab = windTrailList[Random.Range(0, windTrailList.Length - 1)];
        GameObject windTrail = Instantiate(trailPrefab, transformSpawn.position, transform.rotation);
        Destroy(windTrail, 10f);
    }
}
