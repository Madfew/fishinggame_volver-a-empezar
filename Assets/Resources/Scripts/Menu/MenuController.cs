﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public FadeController fadeController;

    public RectTransform imgTitle;
    public RectTransform goButtonCredits;
    public RectTransform goButtonExit;
    public GameObject goButtonStart;

    public GameObject goExitGameplay;
    public Image imgExitGameplay;
    public GameObject goButtonChange;
    public Image imgButtonChange;
    public Image fade;

    public Image imgButtonUp;
    public Image imgButtonDown;

    public Image imgTutorial;
    public Image imgButtonNext;

    void Start()
    {
        fadeController.StartComponents();
        StartCoroutine(DisabledRaycastFade());
    }

    IEnumerator DisabledRaycastFade()
    {
        yield return new WaitForSeconds(1f);
        fadeController.FadeImage(0f);
        yield return new WaitForSeconds(1f);
        fade.raycastTarget = false;
    }

    IEnumerator FadeToExit()
    {
        fadeController.FadeImage(1f);
        yield return new WaitForSeconds(1f);
        Application.Quit();
        yield return new WaitForSeconds(1f);
    }

    IEnumerator ShowButtonExitGameplay()
    {
        yield return new WaitForSeconds(2f);
        goExitGameplay.SetActive(true);
        imgExitGameplay.DOFade(0.6f, 1f);
    }

    IEnumerator ShowButtonChangeGameplay()
    {
        yield return new WaitForSeconds(3f);
        goButtonChange.SetActive(true);
        imgButtonUp.gameObject.SetActive(true);
        imgButtonDown.gameObject.SetActive(true);
        imgButtonChange.DOFade(1f, 1f);
        imgButtonUp.DOFade(1f, 1f);
        imgButtonDown.DOFade(1f, 1f);
    }

    IEnumerator HideButtonChangeGameplay()
    {
        imgButtonChange.DOFade(0f, 1f);
        imgButtonUp.DOFade(0f, 1f);
        imgButtonDown.DOFade(0f, 1f);
        yield return new WaitForSeconds(1f);
        goButtonChange.SetActive(false);
        imgButtonDown.gameObject.SetActive(false);
        imgButtonUp.gameObject.SetActive(false);
    }

    public void StartGameplay()
    {
        goButtonStart.SetActive(false);
        imgTitle.DOAnchorPosY(-350, 0.5f);
        goButtonCredits.DOAnchorPosY(0, 0.5f);
        goButtonExit.DOAnchorPosY(0, 0.5f);
        imgTitle.DOAnchorPosY(400, 1.5f).SetDelay(0.5f);
        goButtonCredits.DOAnchorPosY(-600, 1.5f).SetDelay(0.5f);
        goButtonExit.DOAnchorPosY(-600, 1.5f).SetDelay(0.5f);
        StartCoroutine(IniTutorial());
        //StartCoroutine(ShowButtonExitGameplay());
    }

    public void ShowCredits()
    {
        goButtonStart.SetActive(false);
        fadeController.FadeImage(0.5f);
    }

    public void ExitGame()
    {
        StartCoroutine(FadeToExit());
    }

    public void ShowButtonChangeLaunch()
    {
        StartCoroutine(ShowButtonChangeGameplay());
    }

    public void HideButtonChangeLaunch()
    {
        StartCoroutine(HideButtonChangeGameplay());
    }

    IEnumerator IniTutorial()
    {
        imgTutorial.gameObject.SetActive(true);
        imgButtonNext.gameObject.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        fadeController.FadeImage(0.5f);
        yield return new WaitForSeconds(1f);
        imgTutorial.DOFade(1f, 1f);
        yield return new WaitForSeconds(1f);
        imgButtonNext.DOFade(1f, 1f);
    }

    IEnumerator FinishTutorial()
    {
        imgTutorial.DOFade(0f, 1f);
        imgButtonNext.DOFade(0f, 1f);
        yield return new WaitForSeconds(1f);
        fadeController.FadeImage(0f);
        StartCoroutine(ShowButtonExitGameplay());
        yield return new WaitForSeconds(1f);
        imgTutorial.gameObject.SetActive(false);
        imgButtonNext.gameObject.SetActive(false);
    }

    public void EndTutorial()
    {
        StartCoroutine(FinishTutorial());
    }
}
