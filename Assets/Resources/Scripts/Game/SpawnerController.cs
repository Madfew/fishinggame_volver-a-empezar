﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    public GameObject goFishPrefab;

    private float spawnCooldown;
    private float timeUntilSpawn = 0;

    void Update()
    {
        timeUntilSpawn -= Time.deltaTime;
        if (timeUntilSpawn <= 0)
        {
            SpawnFish();
            spawnCooldown = Random.Range(1.5f, 3f);
            timeUntilSpawn = spawnCooldown;
        }
    }

    public void SpawnFish()
    {
        Vector3 position = new Vector3(transform.position.x, Random.Range(0.4f, 2.6f), Random.Range(-2.2f, 1.2f));
        Instantiate(goFishPrefab, position, goFishPrefab.transform.rotation);
    }
}
