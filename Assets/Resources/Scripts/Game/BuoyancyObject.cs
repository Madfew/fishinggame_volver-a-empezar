﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuoyancyObject : MonoBehaviour
{
    public float underWaterDrag = 3f;
    public float underWaterAngularDrag = 1f;
    public float airDrag = 0f;
    public float airAngularDrag = 0.05f;
    public float floatingPower = 15f;

    public float waterHeight = 0f;

    public float forceJump;

    Rigidbody rb;
    bool underWater;

    float timer = 3f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        float difference = transform.position.y - waterHeight;

        timer -= Time.deltaTime;
        if (timer < 0)
        {
            timer = 1f;
            rb.AddForceAtPosition(Vector3.up * forceJump, transform.position, ForceMode.Force);
        }

        if (difference < 0)
        {
            rb.AddForceAtPosition(Vector3.up * floatingPower * Mathf.Abs(difference), transform.position, ForceMode.Force);
            if (!underWater)
            {
                underWater = true;
                SwitchState(true);
            }
        }
        else if (underWater)
        {
            underWater = false;
            SwitchState(false);
        }
    }

    private void SwitchState(bool isUnderWater)
    {
        if (isUnderWater)
        {
            rb.drag = underWaterDrag;
            rb.angularDrag = underWaterAngularDrag;
        }
        else
        {
            rb.drag = airDrag;
            rb.angularDrag = airAngularDrag;
        }
    }
}
