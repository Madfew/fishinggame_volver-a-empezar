﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FadeController : MonoBehaviour
{
    public Image imgFade;

    public void StartComponents()
    {
        var tempColor = imgFade.color;
        tempColor.a = 1f;
        imgFade.color = tempColor;
    }

    public void FadeImage(float value)
    {
        imgFade.DOFade(value, 1f);
    }
}
