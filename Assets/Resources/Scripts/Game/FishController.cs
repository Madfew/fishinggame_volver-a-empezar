﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishController : MonoBehaviour
{
    //PLAYER ANIMATIONS
    public Animator animPlayer;
    string currentState;

    public bool bDeath;
    bool bFinal;

    void Start()
    {
        StartCoroutine(StartSwimAnim());
    }

    void Update()
    {
        if (bFinal)
        {
            Destroy(this.gameObject);
            return;
        }
        if (bDeath)
        {
            if(transform.position.y >= 4.5f)
            {
                PlayerController.Instance.MakePoint();
                //Debug.Log("Recogio pescado");
                bFinal = true;
                //animPlayer.SetTrigger("catched");
            }
            return;
        }
        transform.position += Vector3.left * Time.deltaTime * 0.95f;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "ColliderDestroy")
        {
            animPlayer.SetTrigger("death");
            Destroy(this.gameObject, 1.5f);
        }
    }

    IEnumerator StartSwimAnim()
    {
        yield return new WaitForSeconds(1.5f);
        ChangeAnimationStage("Swim");
    }

    //-----------ANIMATOR CONTROLLER------------
    public void ChangeAnimationStage(string newState)
    {
        if (currentState == newState) return;
        animPlayer.Play(newState);
        currentState = newState;
    }
}
