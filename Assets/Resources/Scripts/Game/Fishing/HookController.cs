﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class HookController : MonoBehaviour
{
    public float movSpeed = 40f;

    [Range(0, .3f)]
    public float m_MovementSmoothing = .05f;

    private Rigidbody m_Rigidbody;
    private Vector3 m_Velocity = Vector3.zero;

    private float verticalMove = 0f;

    public LineRenderer line;
    public Transform pos1;
    private Transform pos2;

    void Start()
    {
        line.positionCount = 2;
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        LimitMovement();
        verticalMove = CrossPlatformInputManager.GetAxis("Vertical") * movSpeed;
        //Debug.Log(verticalMove);
        pos2 = transform;

        line.SetPosition(0, pos1.position);
        line.SetPosition(1, pos2.position);
    }

    private void FixedUpdate()
    {
        Move(verticalMove * Time.fixedDeltaTime);
    }

    private void Move(float move)
    {
        Vector3 targetVelocity = new Vector2(m_Rigidbody.velocity.x, move * 10f);
        m_Rigidbody.velocity = Vector3.SmoothDamp(m_Rigidbody.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);
    }

    private void LimitMovement()
    {
        float yMovementClamp = Mathf.Clamp(transform.position.y, 0f, 4.5f);
        Vector3 _limitPosition = new Vector3(transform.position.x, yMovementClamp, transform.position.z);
        transform.position = _limitPosition;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Fish")
        {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            other.gameObject.GetComponent<FishController>().bDeath = true;
            other.transform.parent = transform;
            other.transform.rotation = Quaternion.Euler(0, 180, 90);
            other.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            other.GetComponent<Animator>().enabled = false;
            other.transform.localScale = new Vector3(3, 3, 3);
            other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            //Debug.Log("PESCA");
        }
    }
}
