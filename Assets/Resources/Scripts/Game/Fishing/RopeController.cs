﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeController : MonoBehaviour
{
    public GameObject goPrefab, parentObject;
    public GameObject goPrefabHook;
    public GameObject goSpawnPoint;

    [Range(1, 100)]
    public float length = 1;

    public float partDistance = 0.21f;
    public bool bReset, bSpawn, bSnapFirst, bSnapLast;

    private GameObject[] partList;

    void Update()
    {
        if (bReset)
        {
            foreach (GameObject tmp in GameObject.FindGameObjectsWithTag("Player"))
            {
                Destroy(tmp);
            }
        }

        if (bSpawn)
        {
            Spawn();
            parentObject.transform.Find("1").transform.position = goPrefabHook.transform.position;
            parentObject.transform.Find("1").parent = goPrefabHook.transform;
            parentObject.transform.Find((parentObject.transform.childCount + 1).ToString()).transform.position = goSpawnPoint.transform.position;
            parentObject.transform.Find((parentObject.transform.childCount + 1).ToString()).transform.rotation = Quaternion.Euler(-135, 0, 0);
            bSpawn = false;
        }
    }

    public void Spawn()
    {
        int count = (int)(length / partDistance);

        for (int x = 0; x < count; x++)
        {
            GameObject tmp;
            tmp = Instantiate(goPrefab, new Vector3(transform.position.x, transform.position.y + partDistance * (x + 1), transform.position.z), Quaternion.identity, parentObject.transform);
            tmp.transform.eulerAngles = new Vector3(180, 0, 0);
            tmp.name = parentObject.transform.childCount.ToString();

            if (x == 0)
            {
                Destroy(tmp.GetComponent<CharacterJoint>());
                //tmp.transform.position= goPrefabHook.transform.position;
                if (bSnapFirst)
                {
                    tmp.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                }
            }
            else
            {
                tmp.GetComponent<CharacterJoint>().connectedBody = parentObject.transform.Find((parentObject.transform.childCount - 1).ToString()).GetComponent<Rigidbody>();
            }
        }

        if (bSnapLast)
        {
            parentObject.transform.Find((parentObject.transform.childCount).ToString()).GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        }

        FixedPartsRope();
    }

    private void FixedPartsRope()
    {
        partList = new GameObject[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        {
            partList[i] = transform.GetChild(i).gameObject;
        }

        foreach (GameObject go in partList)
        {
            go.gameObject.GetComponent<MeshRenderer>().enabled = false;
        }

        StartCoroutine(ShowParts());
    }

    public void DestroyRope()
    {
        foreach (GameObject go in partList)
        {
            Destroy(go);
        }
        Destroy(goPrefabHook);
    }

    IEnumerator ShowParts()
    {
        yield return new WaitForSeconds(0.2f);
        foreach (GameObject go in partList)
        {
            go.gameObject.GetComponent<MeshRenderer>().enabled = true;
        }
    }
}
