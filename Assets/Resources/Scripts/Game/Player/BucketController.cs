﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BucketController : MonoBehaviour
{
    public GameObject fishStaticPrefab;
    public Transform spawnPoint;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnPoint()
    {
        GameObject fish = Instantiate(fishStaticPrefab, spawnPoint.position, fishStaticPrefab.transform.rotation);
        //fish.GetComponentInChildren<GameObject>().transform.rotation = Quaternion.Euler(new Vector3(Random.Range(-12,12), Random.Range(-12, 12), Random.Range(-12, 12)));
    }
}
