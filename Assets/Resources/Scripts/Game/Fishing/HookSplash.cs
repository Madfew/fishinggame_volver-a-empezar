﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookSplash : MonoBehaviour
{
    public GameObject waterSplash;

    private int enter;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Water")
        {
            waterSplash.SetActive(true);
            if(enter < 1)
            {
                FindObjectOfType<AudioManager>().Play("Hook");
            }
            enter++;
        }
    }
}
